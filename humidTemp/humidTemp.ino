#include <DHT.h>

#define DHT11Pin 2

DHT dht(DHT11Pin, DHT11);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // wait 2 seconds between readings
  delay(2000);
  // get humidity
  float humidity = dht.readHumidity();
  // get temperature as C
  float celsius = dht.readTemperature();
  // get temperature as F
  float fahrenheit = dht.readTemperature(true);
  
  // print results
  Serial.print("Humidity: "); Serial.print(humidity);
  Serial.print(" Celsius: "); Serial.print(celsius);
  Serial.print(" Fahrenheit: "); Serial.println(fahrenheit);
}
